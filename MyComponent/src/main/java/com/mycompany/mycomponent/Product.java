/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author Windows10
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Hot Milk", 40,"1.png"));
        list.add(new Product(2, "Cappuccino Ice", 30,"2.png"));
        list.add(new Product(3, "Hot Green Tea", 40,"3.png"));
        list.add(new Product(4, "Americano Ice", 30,"4.png"));
        list.add(new Product(5, "Green Tea Latte", 40,"5.png"));
        list.add(new Product(6, "Kiwi Smoothie", 50,"6.png"));
        list.add(new Product(7, "Hot Mocha", 40,"7.png"));
        list.add(new Product(8, "Hot tea", 40,"8.png"));
        list.add(new Product(9, "Caramel Macchiato", 40,"9.png"));
        return list;
    }
}
